#!/bin/bash

cdir=`dirname $0`
podomain=kuser
versiune=`cat VERSION`

mode=""
if test -n "$1"; then
    mode=$1
fi
onelang=
if test -n "$2"; then
    onelang=$2
fi

if [[ :all:extract:update-po: != *:$mode:* ]]; then
    echo "*** Unknown mode '$mode'."
    echo "*** (Known modes: all, extract, update-po.)"
    exit 1
fi

    # Creating the file 'kuser.pot'
if test $mode = all || test $mode = extract; then
    echo ">>> Extracting template..."
    cd $cdir
    srcfiles_rc=`find -maxdepth 2 -iname \*.rc | sort`
    srcfiles_kcfg=`find -maxdepth 2 -iname \*.kcfg | sort`
    srcfiles_ui=`find -maxdepth 2 -iname \*.ui | sort`
    srcfiles_src=`find -maxdepth 2 -iname \*.cpp | sort`
    srcfiles="\
        $srcfiles_rc $srcfiles_ui  $srcfiles_kcfg
    "

    # Extracting messages from the *.kcfg, *.rc and *.ui files;
    # and merging them into a single file "rc.cpp" with «extractrc» command
    extractrc  $srcfiles >> rc.cpp

    potfile1=po/1$podomain-new.pot
    potfile2=po/2$podomain-new.pot
    potfile_updated=$podomain.pot

    if [ -f "$potfile1" ] ||  [ -f "$potfile2" ]; then
        rm -f $potfile{1,2}
    fi
    touch $potfile{1,2}

    # Extracting messages from the rc.cpp file
    xgettext -v -C -v -c -j -k'//' -k'i18n' --from-code=utf-8 rc.cpp -o $potfile1
        rm -f rc.cpp

    # Cleaning the '$potfile1' file of lines with useless information
    sed -i -e '/^#:/d' \
        -e '/^#. i18n: ectx:/d' \
        -e 's|. i18n: file||g' \
        -e 's|CHARSET|UTF-8|g' $potfile1

    # Extracting messages from the *.cpp files
    xgettext -v -k'_' -k'N_' -k'//' -k'i18n' --kde -j --add-comments=translators -n --from-code=utf-8 \
     --package-name="kuser" --package-version="$versiune" --msgid-bugs-address="" $potfile1 $srcfiles_src -o $potfile2

    sed -i -e '6d' $potfile2

    sed -i -e "20a #, kde-format\nmsgctxt \"NAME OF TRANSLATORS\"\nmsgid \"Your names\"\nmsgstr \"\"\n\
\n #, kde-format\nmsgctxt \"EMAIL OF TRANSLATORS\"\nmsgid \"Your emails\"\nmsgstr \"\"\n" $potfile2

    rm -f $potfile1
    mv -v $potfile2 $potfile_updated
fi

    # Updating catalogs with recently extracted messages
    # and/or their new reference data
if test $mode = all || test $mode = update-po; then
    echo ">>> Updating catalogs..."
    potfile=$cdir/$podomain.pot
    if [ -z "$onelang" ]; then
        langs=`cat $cdir/LINGUAS | sed 's/#.*//'`
    else
        langs=$onelang
    fi
    for lang in $langs; do
        pofile=$cdir/po/$lang/$podomain.po
        if test ! -f $pofile; then
            echo "--- $pofile is missing."
            continue
        fi
        echo -n "$pofile  "
        msgmerge -U --backup=none --no-wrap --previous $pofile $potfile
    done
fi
