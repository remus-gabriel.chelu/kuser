<?xml version="1.0" ?>
<!DOCTYPE book PUBLIC "-//KDE//DTD DocBook XML V4.2-Based Variant V1.1//EN" 
"dtd/kdex.dtd" [
  <!ENTITY kappname "&kuser;">
  <!ENTITY % addindex "IGNORE">
  <!ENTITY % Estonian  "INCLUDE"
> <!-- change language only here -->
]>

<book id="kuser" lang="&language;">

<bookinfo>
<title
>&kuser;i käsiraamat</title>
<authorgroup>
<author
><firstname
>Matt</firstname
> <surname
>Johnston</surname
> <affiliation
> <address
>&Matt.Johnston.mail;</address>
</affiliation>
</author>

<othercredit role="reviewer"
><firstname
>Lauri</firstname
> <surname
>Watts</surname
> <affiliation
> <address
>&Lauri.Watts.mail;</address>
</affiliation>
<contrib
>Korrigeerija</contrib>
</othercredit>
<othercredit role="reviewer"
><firstname
>Jonathan</firstname
> <surname
>Singer</surname
> <affiliation
> <address
>&Jonathan.Singer.mail;</address>
</affiliation>
<contrib
>Korrigeerija</contrib>
</othercredit>
<othercredit role="translator"
><firstname
>Marek</firstname
><surname
>Laane</surname
><affiliation
><address
><email
>bald@starman.ee</email
></address
></affiliation
><contrib
>Tõlge eesti keelde</contrib
></othercredit
> 
</authorgroup>

<copyright>
<year
>2000</year>
<holder
>&Matt.Johnston;</holder>
</copyright>
<legalnotice
>&FDLNotice;</legalnotice>


<date
>2013-06-22</date>
<releaseinfo
>3.9 (&kde; 4.11)</releaseinfo>

<abstract
><para
>See dokumentatsioon  kirjeldab &kuser;i versiooni 3.9. Rakendus võimaldab hallata süsteemi kasutajaid ja gruppe.</para>
</abstract>

<keywordset>
<keyword
>kuser</keyword>
<keyword
>kasutaja</keyword>
<keyword
>haldus</keyword>
<keyword
>haldamine</keyword>
<keyword
>vahendid</keyword>
<keyword
>grupp</keyword>
<keyword
>parool</keyword>
</keywordset>
</bookinfo>

<chapter id="start">
<title
>Alustamine</title>

<para
>&kuser; võimaldab hallata süsteemi kasutajaid ja gruppe. </para>
<para
>See on &kuser;i lühike sissejuhatus. Lähemat infot leiab peatükist <link linkend="using"
>Kasutamine</link
>.</para>

<para
>Tegelikult pole selleks, et hakata &kuser;it kasutama, vaja mitte kui midagi - välja arvatud <link linkend="customizing"
>seadistamine</link
>.</para>

<para
>Kui sa ei käivita &kuser;it administraatorina, saad hoiatuse, et faili <filename
>/etc/shadow</filename
> ei õnnestunud lugeda.</para>

<!--not in 4.4>
para
>When you have made the changes you want, you must
<guimenuitem
>Save</guimenuitem
> them for them to take effect. Either
choose the Toolbar icon, or use the <guimenu
>File</guimenu
> menu.</para>
-->

</chapter>

<chapter id="using">
<title
>Kasutamine</title>

<sect1 id="sec1mainwindow">
<title
>Peaaken</title>

<para
>&kuser; on väga lihtne rakendus. Põhiaknas on näha kaks kaarti: kasutajate ja gruppide nimekiri. Kasutaja või grupi redigeerimiseks klõpsa lihtsalt selle nimel, mis avab kasutaja või grupi omaduste dialoogi.</para>
<screenshot>
<screeninfo
>&kuser;i peaaken</screeninfo>
<mediaobject>
<imageobject>
<imagedata fileref="kuser.png" format="PNG"/>
</imageobject>
<textobject>
<phrase
>&kuser;i peaaken</phrase>
</textobject>
</mediaobject>
</screenshot>

</sect1>

<sect1 id="properties">
<title
>Kasutaja ja grupi omadused</title>

<sect2 id="user-properties">
<title
>Kasutaja omaduste dialoog</title>

<para
>Kasutaja omaduste dialoogil on kaks kaarti.</para>

<!-- is this still valid for 4.4?
<para
>The number of tabs depends on the type of the user storage system
and whether quotas are being used. Additional tabs will appear if you
have shadow passwords, or any other similar things such as
<filename
>/etc/master.passwd</filename
> in BSD flavor Unices.</para>
-->
<sect3 id="user-info">
<title
>Kaart Kasutaja info</title>

<para
>Kaardil <guilabel
>Kasutaja info</guilabel
> saab muuta järgmisi elemente: </para>

<itemizedlist>
<listitem
><para
><guilabel
>Kasutajanime</guilabel
> juures kasutajanime ja nupuga <guibutton
>Määra parool</guibutton
> konto parooli,</para
></listitem>
<listitem
><para
><guilabel
>Kasutaja ID</guilabel
></para
></listitem>
<listitem
><para
><guilabel
>Täielik nimi</guilabel
></para
></listitem>
<listitem
><para
><guilabel
>Shell</guilabel
> (sisselogimisshellide nimekiri hangitakse failist <filename
>/etc/shells</filename
>)</para
></listitem>
<listitem
><para
><guilabel
>Kodukataloog</guilabel
></para
></listitem>
<listitem
><para
>Kaks kontori asukohta</para
></listitem>
<listitem
><para
><guilabel
>Aadress</guilabel
></para
></listitem>
<listitem
><para
><guilabel
>Konto suletud</guilabel
></para
></listitem>
</itemizedlist>

</sect3>

<sect3 id="password-management-info">
<title
>Kaart Paroolide haldamine</title>

<para
>Kaart <guilabel
>Paroolide haldamine</guilabel
> on administraatori režiimis näha siis, kui kasutusel on variparoolid (shadow) või näiteks <acronym
>BSD</acronym
> tüüpi UNIX süsteemides kasutatav <filename
>/etc/master.passwd</filename
>.</para>

<!--para
>In the <guilabel
>Extended Info</guilabel
> tab you can modify
parameters related to extended account control: </para-->

<itemizedlist>
<listitem
><para
>Minimaalne päevade arv parooli muutmise vahel</para
></listitem>
<listitem
><para
>Päevade arv, pärast mida parool aegub, kui seda ei ole muudetud</para
></listitem>
<listitem
><para
>Päevade arv enne aegumist, mil kasutajat hoiatatakse</para
></listitem>
<listitem
><para
>Kas ja millal konto suletakse, kui parool on aegunud</para
></listitem>
<listitem
><para
>Päev, mil konto aegub</para
></listitem>
<listitem
><para
><guilabel
>Klass</guilabel
> (<acronym
>BSD</acronym
> süsteemid)</para
></listitem>
</itemizedlist>
<para
>Parooli viimase muutmise kuupäeva näidatakse dialoogi ülaosas.</para>


</sect3>
<!-- no quota in kde 3.5 - 4.11, 
was removed with http://lists.kde.org/?l=kde-commits&m=108486541029598&w=2
<sect2 id="quota">
<title
><guilabel
>Quota</guilabel
> Tab</title>

<para
>You will probably see the <guilabel
>Quota</guilabel
> tab only if you
have at least one mounted volume with quota enabled and a quota file
present. There you may modify all quota related parameters: </para>

<itemizedlist>
<listitem
><para
><guilabel
>File Soft Quota</guilabel
></para
></listitem>
<listitem
><para
><guilabel
>File Hard Quota</guilabel
></para
></listitem>
<listitem
><para
><guilabel
>File Time Limit (Grace 
Period)</guilabel
></para
></listitem>
<listitem
><para
><guilabel
>iNode Soft Quota</guilabel
></para
></listitem>
<listitem
><para
><guilabel
>iNode Hard Quota</guilabel
></para
></listitem>
<listitem
><para
><guilabel
>iNode Time Limit (Grace 
Period)</guilabel
></para
></listitem>
</itemizedlist>

<para
> All these parameters can be changed for each file system that has user
quota enabled. File systems can be changed using the <guilabel
>Quota
Filesystem</guilabel
> box.</para>

</sect2>
-->
<sect3 id="group">
<title
>Kaart Grupid</title>

<para
>Kaardil <guilabel
>Grupid</guilabel
> on näha kogu info valitud kasutaja kuulumise kohta gruppidesse. Esmase grupi, kuhu kasutaja kuulub, saab määrata nupuga <guilabel
>Määra esmagrupiks</guilabel
>. Kasutajat saab lisada suures kastis vastavaid gruppe märkides ka teistesse gruppidesse.</para>

</sect3>
</sect2>

<sect2 id="group-properties">
<title
>Grupi omadused</title>

<para
>Dialoog <guilabel
>Grupi omadused</guilabel
> sisaldab kõigi valitud grupi kasutajate nimekirja vasakul ning kõigi teiste kasutajate nimekirja.</para
> 
<para
>Vali kasutaja ühes nimekirjas ning kasuta grupi liikmete haldamiseks nuppe <guibutton
>Lisa</guibutton
> ja <guibutton
>Eemalda</guibutton
>.</para>

</sect2>

</sect1>

<sect1 id="managing-users-groups">
<title
>Kasutajate või gruppide haldamine</title>

<para
>Kasutaja või grupi lisamiseks süsteemi vali menüüst <guimenu
>Kasutaja</guimenu
> või <guimenu
>Grupp</guimenu
> käsk <guimenuitem
>Lisa...</guimenuitem
> või klõpsa tööriistaribal nupul <guiicon
>Lisa</guiicon
>.</para>
<para
>Valitud kasutajat või gruppi saab samamoodi muuta või kustutada.</para>
</sect1>
</chapter>

<chapter id="customizing">
<title
>&kuser;i kohandamine</title>

<sect1 id="defaults">
<title
>Kasutaja loomise vaikeväärtuste muutmine</title>

<para
>Kasutaja loomise vaikeväärtuste muutmiseks kasuta kaarti <guilabel
>Ühendus</guilabel
> &kuser;i seadistustedialoogi lehel <guilabel
>Üldine</guilabel
>, mille saab avada menüükäsuga <menuchoice
><guimenu
>Seadistused</guimenu
> <guimenuitem
>&kuser;i seadistamine...</guimenuitem
></menuchoice
>. </para
> 
<para
>Seal saab muuta vaikeväärtusi, mida kasutatakse uue kasutaja loomisel: shell ja kodukataloog. Saab valida ka selle, kas <quote
>loomisel</quote
> tekitatakse kodukataloog või mitte ning kas sellesse kopeeritakse tüüpfailid (standardsed seadistustefailid) või mitte. Samuti saab lubada <guilabel
>kasutaja privaatgrupi</guilabel
>, mis tekitab koos uue kasutaja loomisega uue isikliku grupi ning kasutaja eemaldamisel kustutab selle.</para>
<!--not in kde 4.4
<para
>The skeleton files for new users can be specified in the <guilabel
>Sources
</guilabel
> tab.</para>
-->
<!--FIXME missing
Password Policy tab 
Times for password changes and expiration of passwords and accounts

Files page
Location of user and group data files
shadow passwords

LDAP page preferences for users in a local network 
Connection tab
Settings tabs
Samba tabs
  -->
</sect1>
</chapter>

<chapter id="credits">
<title
>Autorid ja litsents</title>

<para
>&kuser;</para>

<para
>Rakenduse autoriõigus 1997-2000: Denis Pershin <email
>dyp@inetlab.com</email
></para>

<para
>Dokumentatsiooni autoriõigus 1997-2000: Denis Pershin <email
>dyp@inetlab.com</email
></para>
<para
>Dokumentatsiooni autoriõigus 2000: &Matt.Johnston; &Matt.Johnston.mail;</para>
<para
>Tõlge eesti keelde Marek Laane <email
>bald@starman.ee</email
></para
> 
&underFDL; &underGPL; </chapter>
&documentation.index; 

</book>
<!--
Local Variables:
mode: sgml
sgml-omittag: nil
sgml-shorttag: t
End:
-->

